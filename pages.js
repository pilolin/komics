const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/category.pug',
		filename: './category.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/episode.pug',
		filename: './episode.html'
	}),
]