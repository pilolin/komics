import MicroModal from 'micromodal';
import lozad from 'lozad';
import Swiper, { Navigation } from 'swiper';
import './episode-page';
import '../scss/styles.scss';

Swiper.use([Navigation]);

window.Swiper = Swiper;

function debounce(f, ms) {
    var isCooldown = false;
    return function() {
        if (isCooldown) return;
        f.apply(this, arguments);
        isCooldown = true;
        setTimeout(function () {
            isCooldown = false;
        }, ms);
    };

}

window.getCookie = function(name) {
    const matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

window.setCookie = function(name, value, options = {}) {

    options = {
        path: '/',
        ...options
    };

    if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (const optionKey in options) {
        updatedCookie += "; " + optionKey;
        const optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

window.deleteCookie = function(name) {
    setCookie(name, "", { 'max-age': -1 });
}

window.addEventListener('load', () => {
    if (document.querySelector('.sounds-tabs')) {
        new Swiper('.sounds-tabs .swiper-container', {
            spaceBetween: 8,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '.sounds-tabs .swiper-button-next',
                prevEl: '.sounds-tabs .swiper-button-prev',
            },
        });
    }
})

lozad('.lazy', {
    loaded: function(el) {
        el.classList.add('loaded');
    },
}).observe();

MicroModal.init({
    openTrigger: 'data-micromodal-open',
    closeTrigger: 'data-micromodal-close',
    openClass: 'is-open',
    disableScroll: true,
    awaitCloseAnimation: true,
    onClose: function(modal) {
        setTimeout(function() {
            const iframe = modal.querySelector('iframe');
            iframe.src = iframe.src;
        }, 320);
    },
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.btn-menu-mobile')) return;

    e.preventDefault();

    document.body.classList.toggle('menu-open');
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.box-close-menu')) return;

    e.preventDefault();

    document.body.classList.remove('menu-open');
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.btn-menu')) return;

    e.preventDefault();

    e.target.closest('.btn-menu').classList.toggle('active');
    document.querySelector('.drop-header').classList.toggle('active');
    document.querySelector('.close-menu-field').classList.toggle('active');
    document.querySelector('.social-block').classList.remove('open');
});

document.addEventListener('click', function(e) {
    if (!e.target.closest('.close-menu-field')) return;

    e.preventDefault();

    e.target.closest('.close-menu-field').classList.remove('active', 'open');
    document.querySelector('.drop-header').classList.remove('active');
    document.querySelector('.btn-menu').classList.remove('active');
    document.querySelector('.social-block').classList.remove('open');
});


const scrollToBtn = document.querySelector('.scrollToBtn');

if (scrollToBtn) {
    document.addEventListener('click', function(e) {
        const targetLink = e.target.closest('.scrollToBtn');

        if (!targetLink) return;

        e.preventDefault();

        window.scrollTo({
            top: pageYOffset + document.querySelector(targetLink.hash).getBoundingClientRect().top,
            left: 0,
            behavior: 'smooth'
        });
    });
}

// anchor scroll
var DELTA = 20;
window.addEventListener('load', () => {
    if (location.hash && document.querySelector(location.hash)) {
        scrollAnchor(document.querySelector(location.hash));
    }
});

document.addEventListener('click', function (e) {
    if (e.target.closest('a[href^="#"]') || e.target.closest('a[href^="/#"]')) {

        const link = e.target.closest('a[href^="#"],a[href^="/#"]');

        let hash = link.hash;

        if (!hash) return;

        const element = document.querySelector(hash);

        if (!element) return;

        e.preventDefault();

        history.pushState(null, document.title, hash);

        scrollAnchor(document.querySelector(hash));
    }
})

function scrollAnchor(targetElem) {
    const headerHeight = document.querySelector('header').scrollHeight;

    const top = targetElem.getBoundingClientRect().top + window.pageYOffset - headerHeight - DELTA;

    window.scrollTo({top, behavior: 'smooth'});
}

window.addEventListener('load', () => {
    if (document.querySelector('.recommend-serials-slider')) {
        new Swiper('.recommend-serials-slider', {
            spaceBetween: 12,
            slidesPerView: 2.5,
            breakpoints: {
                768: {
                    slidesPerView: 3.5,
                    spaceBetween: 26,
                },
                992: {
                    slidesPerView: 5,
                    spaceBetween: 26,
                },
            }
        });
    }
})

window.addEventListener('load', () => {
    if (document.querySelector('.category-seasons')) {
        const activeElement = document.querySelector('.category-seasons .item[aria-selected="true"]');
        const initialSlide = activeElement ? [...activeElement.parentElement.parentElement.children].indexOf(activeElement.parentElement) : 0;

        new Swiper('.category-seasons .swiper-container', {
            centeredSlides: true,
            spaceBetween: 8,
            initialSlide,
            slidesPerView: 'auto',
        });
    }
});

window.addEventListener('load', () => {
    if (document.querySelector('.all-serials')) {
        var allSerialsTabs = document.querySelector('.all-serials .tabs .swiper-container');

        function initAllSerialsTabsSlider() {
            if (window.matchMedia('(max-width: 768px)').matches) {
                new Swiper('.all-serials .swiper-container', {
                    centeredSlides: false,
                    spaceBetween: 16,
                    slidesPerView: 'auto',
                });
            } else {
                if(allSerialsTabs.swiper) {
                    allSerialsTabs.swiper.destroy(false, true);
                }
            }

        }

        initAllSerialsTabsSlider();

        window.addEventListener('resize', initAllSerialsTabsSlider)
    }
});

var votingContainer = document.querySelector('.voting');
if (votingContainer) {
    var stars = votingContainer.querySelectorAll('.vote');

    votingContainer.addEventListener('mouseover', function (e) {
        var currentStar = e.target.closest('.vote');
        if (!currentStar) return;

        for(var i = 0; i < stars.length; i++) {
            if(stars[i].dataset.rating <= currentStar.dataset.rating) {
                stars[i].classList.add('hover');
            } else {
                stars[i].classList.remove('hover');
            }
        }
    });

    votingContainer.addEventListener('mouseout', function (e) {
        for(var i = 0; i < stars.length; i++) {
            stars[i].classList.remove('hover');
        }
    });
}